# README #

### What is this repository for? ###

* Quickstart your project with Bootstrap CDN, SCSS, CSS Wizardry Grid, JS Lint, Uglifier and browser-sync
* Version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* NodeJS/NPM needs to be installed
* after Downloading or cloning this repository you navigate to the root folder and start npm install
* when you start working you have to start the watch tasks with npm run watch:all
* open the browser and go to http://localhost:3000
* change some files like the index.html or some scss files and you will see the browser reloading automatically with your changes
* have fun

### Who do I talk to? ###

* Phuc Le
* Code Hard Play Hard Meetup

### Known Issues ###

* Watching SCSS doesn't work on Windows at the moment - fixed
* Doesn't work on a friends mac, that's strange